defmodule Cacophony.MessageTest do
  use ExUnit.Case

  @whoami_oid "1.3.6.1.4.1.4203.1.11.3"
  @cirno_oid "9.99.999.9999.99999.999999"

  describe "decode/1" do
    test "successfully decodes a bindRequest" do
      bindreq = {:BindRequest, 3, "cn=foo", {:simple, "bar"}}
      message = {:LDAPMessage, 1, {:bindRequest, bindreq}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.BindRequest{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 1
      assert msg.dn == "cn=foo"
      assert msg.credentials == {:simple, "bar"}
    end

    test "successfully decodes a bindResponse" do
      bindresp = {:BindResponse, :success, "cn=foo", "", :asn1_NOVALUE, :asn1_NOVALUE}
      message = {:LDAPMessage, 1, {:bindResponse, bindresp}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.BindResponse{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 1
      assert msg.matched_dn == "cn=foo"
      assert msg.result_code == :success
    end

    test "successfully decodes an unbindRequest" do
      message = {:LDAPMessage, 1, {:unbindRequest, :asn1_NOVALUE}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.UnbindRequest{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 1
    end

    test "successfully decodes an extended whoAmIRequest" do
      message =
        {:LDAPMessage, 1, {:extendedReq, {:ExtendedRequest, @whoami_oid, :asn1_NOVALUE}},
         :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.WhoAmIRequest{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 1
    end

    test "successfully decodes an unknown extendedRequest" do
      message =
        {:LDAPMessage, 1, {:extendedReq, {:ExtendedRequest, @cirno_oid, :asn1_NOVALUE}},
         :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.ExtendedRequest{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 1
    end

    test "successfully decodes a searchRequest" do
      searchreq =
        {:SearchRequest, "", :baseObject, :neverDerefAliases, 0, 0, false,
         {:present, "objectclass"}, ["supportedSASLMechanisms"]}

      message = {:LDAPMessage, 1, {:searchRequest, searchreq}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.SearchRequest{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 1
      assert msg.attributes == ["supportedSASLMechanisms"]
      assert msg.filter == {:present, "objectclass"}
      assert msg.base == ""
      assert msg.scope == :baseObject
      assert msg.deref_aliases == :neverDerefAliases
      assert msg.time_limit == 0
      assert msg.size_limit == 0
      refute msg.types_only
    end

    test "successfully decodes a searchResDone" do
      searchresdone = {:SearchResultDone, :success, "", "", :asn1_NOVALUE}
      message = {:LDAPMessage, 1, {:searchResDone, searchresdone}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.SearchResultDone{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 1
    end

    test "successfully decodes a searchResEntry" do
      searchresentry =
        {:SearchResultEntry, "cn=test", [{:PartialAttribute, "key", ["value1", "value2"]}]}

      message = {:LDAPMessage, 1, {:searchResEntry, searchresentry}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.SearchResultEntry{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 1
      assert msg.object_name == "cn=test"

      assert msg.attributes == [
               %Cacophony.Message.PartialAttribute{name: "key", values: ["value1", "value2"]}
             ]
    end

    test "successfully decodes an abandonRequest" do
      message = {:LDAPMessage, 2, {:abandonRequest, 1}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.AbandonRequest{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 2
      assert msg.abandon_id == 1
    end

    test "successfully decodes an intermediateResponse" do
      ir = {:IntermediateResponse, @cirno_oid, "9 is the best number"}
      message = {:LDAPMessage, 1, {:intermediateResponse, ir}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.IntermediateResponse{} = msg} = Cacophony.Message.decode(bin)

      assert msg.id == 1
      assert msg.response_name == @cirno_oid
      assert msg.response_value == "9 is the best number"
    end
  end

  describe "encode/1" do
    test "successfully encodes a bindRequest" do
      bindreq = {:BindRequest, 3, "cn=foo", {:simple, "bar"}}
      message = {:LDAPMessage, 1, {:bindRequest, bindreq}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.BindRequest{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end

    test "successfully encodes a bindResponse" do
      bindresp = {:BindResponse, :success, "cn=foo", "", :asn1_NOVALUE, :asn1_NOVALUE}
      message = {:LDAPMessage, 1, {:bindResponse, bindresp}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.BindResponse{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end

    test "successfully encodes an unbindRequest" do
      message = {:LDAPMessage, 1, {:unbindRequest, :asn1_NOVALUE}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.UnbindRequest{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end

    test "successfully encodes an extended whoAmIRequest" do
      message =
        {:LDAPMessage, 1, {:extendedReq, {:ExtendedRequest, @whoami_oid, :asn1_NOVALUE}},
         :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.WhoAmIRequest{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end

    test "successfully encodes an unknown extendedRequest" do
      message =
        {:LDAPMessage, 1, {:extendedReq, {:ExtendedRequest, @cirno_oid, :asn1_NOVALUE}},
         :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.ExtendedRequest{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end

    test "successfully encodes a searchRequest" do
      searchreq =
        {:SearchRequest, "", :baseObject, :neverDerefAliases, 0, 0, false,
         {:present, "objectclass"}, ["supportedSASLMechanisms"]}

      message = {:LDAPMessage, 1, {:searchRequest, searchreq}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.SearchRequest{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end

    test "successfully encodes a searchResDone" do
      searchresdone = {:SearchResultDone, :success, "", "", :asn1_NOVALUE}
      message = {:LDAPMessage, 1, {:searchResDone, searchresdone}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.SearchResultDone{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end

    test "successfully encodes a searchResEntry" do
      searchresentry =
        {:SearchResultEntry, "cn=test", [{:PartialAttribute, "key", ["value1", "value2"]}]}

      message = {:LDAPMessage, 1, {:searchResEntry, searchresentry}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.SearchResultEntry{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end

    test "successfully encodes an abandonRequest" do
      message = {:LDAPMessage, 2, {:abandonRequest, 1}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.AbandonRequest{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end

    test "successfully encodes an intermediateResponse" do
      ir = {:IntermediateResponse, @cirno_oid, "9 is the best number"}
      message = {:LDAPMessage, 1, {:intermediateResponse, ir}, :asn1_NOVALUE}

      {:ok, bin} = :LDAP.encode(:LDAPMessage, message)

      {:ok, %Cacophony.Message.IntermediateResponse{} = msg} = Cacophony.Message.decode(bin)

      {:ok, other_bin} = Cacophony.Message.encode(msg)

      assert bin == other_bin
    end
  end
end
